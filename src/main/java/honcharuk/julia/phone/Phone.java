package honcharuk.julia.phone;

public abstract class Phone {
    protected String name;
    protected String model;
    protected String storageVolume;
    protected String ramVolume;

    public String getName() {
        return name;
    }

    public String getStorageVolume() {
        return storageVolume;
    }

    public String getModel() {
        return model;
    }

    public String getRamVolume() {
        return ramVolume;
    }

    @Override
    public String toString() {
        return "Телефон марки " + getName() + " об'ємом " + getStorageVolume();
    }
}
