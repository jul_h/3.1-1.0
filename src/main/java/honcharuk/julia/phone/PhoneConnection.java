package honcharuk.julia.phone;

public interface PhoneConnection {
    void call();
    void sendMessage();
}
