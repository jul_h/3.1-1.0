package honcharuk.julia.phone;

public interface PhoneMedia {
    void makePhoto();
    void makeVideo();
}
