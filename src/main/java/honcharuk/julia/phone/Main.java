package honcharuk.julia.phone;

public class Main {
    public static void main(String[] args) {
        SamsungPhone samsungPhone = new SamsungPhone("Galaxy", "256 ГБ", "8 ГБ");
        System.out.println(samsungPhone.getName());
        System.out.println(samsungPhone.getModel());
        System.out.println(samsungPhone.getStorageVolume());
        System.out.println(samsungPhone.getRamVolume());
        samsungPhone.call();
        samsungPhone.sendMessage();
        samsungPhone.makePhoto();
        samsungPhone.makeVideo();
        System.out.println(samsungPhone);

        NokiaPhone nokiaPhone = new NokiaPhone("Lumia", "128 ГБ", "4 ГБ" );
        System.out.println(nokiaPhone.getName());
        System.out.println(nokiaPhone.getModel());
        System.out.println(nokiaPhone.getStorageVolume());
        System.out.println(nokiaPhone.getRamVolume());
        nokiaPhone.call();
        nokiaPhone.sendMessage();
        System.out.println(nokiaPhone);
    }
}
