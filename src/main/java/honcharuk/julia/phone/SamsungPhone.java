package honcharuk.julia.phone;

public class SamsungPhone extends Phone implements PhoneConnection, PhoneMedia {

    private static final String SAMSUNG_NAME = "Samsung";

    public SamsungPhone(String model, String storageVolume, String ramVolume) {
        this.model = model;
        this.storageVolume = storageVolume;
        this.ramVolume = ramVolume;
        this.name = SAMSUNG_NAME;
    }

    public void call() {
        System.out.println("Здійснюється виклик");
    }
    public void sendMessage() {
        System.out.println("Надсилається повідомлення");
    }
    public void makePhoto() {
        System.out.println("Створюється фото");
    }
    public void makeVideo() {
        System.out.println("Створюється відео");
    }
}

