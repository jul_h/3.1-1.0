package honcharuk.julia.phone;

public class NokiaPhone extends Phone implements PhoneConnection {

    private static final String NOKIA_NAME = "Nokia";

    public NokiaPhone(String model, String storageVolume, String ramVolume) {
        this.model = model;
        this.storageVolume = storageVolume;
        this.ramVolume = ramVolume;
        this.name = NOKIA_NAME;
    }

    public void call() {
        System.out.println("Здійснюється виклик");
    }

    public void sendMessage() {
        System.out.println("Надсилається повідомлення");
    }
}

